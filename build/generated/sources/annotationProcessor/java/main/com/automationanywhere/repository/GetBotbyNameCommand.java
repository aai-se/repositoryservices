package com.automationanywhere.repository;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.BotCommand;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import java.lang.ClassCastException;
import java.lang.Deprecated;
import java.lang.Object;
import java.lang.String;
import java.lang.Throwable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class GetBotbyNameCommand implements BotCommand {
  private static final Logger logger = LogManager.getLogger(GetBotbyNameCommand.class);

  private static final Messages MESSAGES_GENERIC = MessagesFactory.getMessages("com.automationanywhere.commandsdk.generic.messages");

  @Deprecated
  public Optional<Value> execute(Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return execute(null, parameters, sessionMap);
  }

  public Optional<Value> execute(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    logger.traceEntry(() -> parameters != null ? parameters.entrySet().stream().filter(en -> !Arrays.asList( new String[] {}).contains(en.getKey()) && en.getValue() != null).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)).toString() : null, ()-> sessionMap != null ?sessionMap.toString() : null);
    GetBotbyName command = new GetBotbyName();
    HashMap<String, Object> convertedParameters = new HashMap<String, Object>();
    if(parameters.containsKey("sessionName") && parameters.get("sessionName") != null && parameters.get("sessionName").get() != null) {
      convertedParameters.put("sessionName", parameters.get("sessionName").get());
      if(convertedParameters.get("sessionName") !=null && !(convertedParameters.get("sessionName") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","sessionName", "String", parameters.get("sessionName").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("sessionName") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","sessionName"));
    }

    if(parameters.containsKey("path") && parameters.get("path") != null && parameters.get("path").get() != null) {
      convertedParameters.put("path", parameters.get("path").get());
      if(convertedParameters.get("path") !=null && !(convertedParameters.get("path") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","path", "String", parameters.get("path").get().getClass().getSimpleName()));
      }
    }

    if(parameters.containsKey("name") && parameters.get("name") != null && parameters.get("name").get() != null) {
      convertedParameters.put("name", parameters.get("name").get());
      if(convertedParameters.get("name") !=null && !(convertedParameters.get("name") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","name", "String", parameters.get("name").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("name") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","name"));
    }

    if(parameters.containsKey("workspacetype") && parameters.get("workspacetype") != null && parameters.get("workspacetype").get() != null) {
      convertedParameters.put("workspacetype", parameters.get("workspacetype").get());
      if(convertedParameters.get("workspacetype") !=null && !(convertedParameters.get("workspacetype") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","workspacetype", "String", parameters.get("workspacetype").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("workspacetype") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","workspacetype"));
    }
    if(convertedParameters.get("workspacetype") != null) {
      switch((String)convertedParameters.get("workspacetype")) {
        case "SELECT" : {
          if(parameters.containsKey("workspaceSelect") && parameters.get("workspaceSelect") != null && parameters.get("workspaceSelect").get() != null) {
            convertedParameters.put("workspaceSelect", parameters.get("workspaceSelect").get());
            if(convertedParameters.get("workspaceSelect") !=null && !(convertedParameters.get("workspaceSelect") instanceof String)) {
              throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","workspaceSelect", "String", parameters.get("workspaceSelect").get().getClass().getSimpleName()));
            }
          }
          if(convertedParameters.get("workspaceSelect") == null) {
            throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","workspaceSelect"));
          }
          if(convertedParameters.get("workspaceSelect") != null) {
            switch((String)convertedParameters.get("workspaceSelect")) {
              case "public" : {

              } break;
              case "private" : {

              } break;
              default : throw new BotCommandException(MESSAGES_GENERIC.getString("generic.InvalidOption","workspaceSelect"));
            }
          }


        } break;
        case "STRING" : {
          if(parameters.containsKey("workspaceString") && parameters.get("workspaceString") != null && parameters.get("workspaceString").get() != null) {
            convertedParameters.put("workspaceString", parameters.get("workspaceString").get());
            if(convertedParameters.get("workspaceString") !=null && !(convertedParameters.get("workspaceString") instanceof String)) {
              throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","workspaceString", "String", parameters.get("workspaceString").get().getClass().getSimpleName()));
            }
          }
          if(convertedParameters.get("workspaceString") == null) {
            throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","workspaceString"));
          }


        } break;
        default : throw new BotCommandException(MESSAGES_GENERIC.getString("generic.InvalidOption","workspacetype"));
      }
    }

    command.setSessions(sessionMap);
    try {
      Optional<Value> result =  Optional.ofNullable(command.action((String)convertedParameters.get("sessionName"),(String)convertedParameters.get("path"),(String)convertedParameters.get("name"),(String)convertedParameters.get("workspacetype"),(String)convertedParameters.get("workspaceSelect"),(String)convertedParameters.get("workspaceString")));
      return logger.traceExit(result);
    }
    catch (ClassCastException e) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.IllegalParameters","action"));
    }
    catch (BotCommandException e) {
      logger.fatal(e.getMessage(),e);
      throw e;
    }
    catch (Throwable e) {
      logger.fatal(e.getMessage(),e);
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.NotBotCommandException",e.getMessage()),e);
    }
  }

  public Map<String, Value> executeAndReturnMany(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return null;
  }
}
