/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 *
 */
package com.automationanywhere.repository;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.data.impl.TableValue;
import com.automationanywhere.botcommand.data.model.Schema;
import com.automationanywhere.botcommand.data.model.table.Row;
import com.automationanywhere.botcommand.data.model.table.Table;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.repository.utils.CRConnection;
import com.automationanywhere.repository.utils.CROperations;

import static com.automationanywhere.commandsdk.model.AttributeType.SELECT;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

/** 
 * @author Stefan Karsten
 */
@BotCommand
@CommandPkg(
		//Unique name inside a package and label to display.
		name = "GetBotStructure", label = "Get Bot Structure", 
		node_label = "Get Bot Structure", description = "Get Bot Structure", comment = true ,  text_color = "#83b9c9" , background_color =  "#83b9c9" , icon = "pkg.svg", 
		return_label = "Structure", return_type = DataType.ANY, return_sub_type = STRING ,return_required = true, return_description = "Dictionary for Packages, Table for Commands")

public class GetBotStructure {
	
    @Sessions
    private Map<String, Object> sessions;
  
	
	private static final Messages MESSAGES = MessagesFactory
			.getMessages("com.automationanywhere.botcommand.samples.messages");

	//Identify the entry point for the action. Returns a Value<String> because the return type is String. 
	@Execute
	public Value action(@Idx(index = "1", type = TEXT)  @Pkg(label = "Session name" , default_value_type = STRING , default_value = "Default") @NotEmpty String sessionName,
	          						@Idx(index = "2", type = TEXT)  @Pkg(label = "Bot Content (JSON)" , default_value_type = STRING ) @NotEmpty String json,
	          						@Idx(index = "3", type = SELECT, options = {
	          								@Idx.Option(index = "3.1", pkg = @Pkg(label = "Commands", value = "nodes")),
	          								@Idx.Option(index = "3.2", pkg = @Pkg(label = "Packages", value = "packages"))
	          							}) @Pkg(label = "Type", default_value = "commands", default_value_type = STRING) @NotEmpty String details) throws Exception 
    {

		LinkedHashMap<String,Value> itemMap = new LinkedHashMap<String,Value>();
		Value result = new StringValue("");
		
		JSONObject botlogic = new JSONObject(json);
		if (botlogic != null) {
			
			JSONArray sublogic = botlogic.getJSONArray(details);
			if (sublogic != null) {
				if (details.equals("packages")) {
					sublogic.forEach(item -> {
		    			JSONObject jsonobj = (JSONObject) item;
						String packagename = jsonobj.getString("name");
						String version = jsonobj.getString("version");
						itemMap.put(packagename, new StringValue(version));
		    	  });
					DictionaryValue dictValue = new DictionaryValue();
					dictValue.set(itemMap);
					result = dictValue;

				}
				
				if (details.equals("nodes")) {
					TableValue tableValue= new TableValue();
					Table table = new Table();
					List<Row> rows = new ArrayList<Row>();
					List<Schema> schemas = new ArrayList<Schema>();
					Schema schema =  new Schema();
					schema.setName("Command Name"); 
					schemas.add(schema);
					schema =  new Schema();
					schema.setName("Package Name"); 
					schemas.add(schema);
					schema =  new Schema();
					schema.setName("Content"); 
					schemas.add(schema);
					table.setSchema(schemas);
					parseNodeArray(rows,sublogic);
					table.setRows(rows);
					table.setSchema(schemas);
					tableValue.set(table);
					result = tableValue;
				}
			}		
			
		}
		
		return result;
		
    }
	
	
	private void parseNodeArray(List<Row> rows,JSONArray sublogic) {
		
		List<Object> commands= sublogic.toList();
		sublogic.forEach(item -> {
			JSONObject jsonobj = (JSONObject) item;
			if (jsonobj.has("commandName") || jsonobj.has("triggerName")) {
				String packageName = jsonobj.getString("packageName");
				String commandName = (jsonobj.getString("commandName") == null) ? jsonobj.getString("triggerName") : jsonobj.getString("commandName");
				List<Value> values = new ArrayList<Value>();
				values.add(new StringValue(commandName));
				values.add(new StringValue(packageName));
				values.add(new StringValue(jsonobj.toString()));
				rows.add(new Row(values));
				if (jsonobj.has("branches")) {
					JSONArray childsublogic = jsonobj.getJSONArray("branches");
					parseNodeArray(rows,childsublogic);
				}
				if (jsonobj.has("children")) {
					JSONArray childsublogic = jsonobj.getJSONArray("children");
					parseNodeArray(rows,childsublogic);
				}
				
				if (jsonobj.has("triggers")) {
					JSONArray childsublogic = jsonobj.getJSONArray("triggers");
					parseNodeArray(rows,childsublogic);
				}
			}
		});
	}

	


	public void setSessions(Map<String, Object> sessions) {
		this.sessions = sessions;
	}
	
	
	
	
}
