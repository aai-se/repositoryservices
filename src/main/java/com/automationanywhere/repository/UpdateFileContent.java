/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 *
 */
package com.automationanywhere.repository;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.repository.utils.CRConnection;
import com.automationanywhere.repository.utils.CROperations;

import static com.automationanywhere.commandsdk.model.AttributeType.SELECT;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;

/** 
 * @author Stefan Karsten
 */
@BotCommand
@CommandPkg(
		//Unique name inside a package and label to display.
		name = "UpdateFileContent", label = "Update File Content", 
		node_label = "Update File Content", description = "Update File JSON Content", comment = true ,  text_color = "#83b9c9" , background_color =  "#83b9c9" , icon = "pkg.svg", 
		return_label = "Result", return_type = STRING, return_required = false)

public class UpdateFileContent {
	
    @Sessions
    private Map<String, Object> sessions;
  
	
	private static final Messages MESSAGES = MessagesFactory
			.getMessages("com.automationanywhere.botcommand.samples.messages");

	//Identify the entry point for the action. Returns a Value<String> because the return type is String. 
	@Execute
	public StringValue action(@Idx(index = "1", type = TEXT)  @Pkg(label = "Session name" , default_value_type = STRING , default_value = "Default") @NotEmpty String sessionName,
	          			  	  @Idx(index = "2", type = TEXT)  @Pkg(label = "File ID" , default_value_type = STRING ) @NotEmpty String id ,
	          			  	  @Idx(index = "3", type = TEXT)  @Pkg(label = "Content" , default_value_type = STRING ) @NotEmpty String content) throws Exception 
    {

		CRConnection connection  = (CRConnection) this.sessions.get(sessionName);  
		CROperations crOp = new CROperations().
							setCrurl(connection.url).
							setToken(connection.token);
		
		String result = crOp.updateJSONContent(id, content);

		return new StringValue(result);

    }


	public void setSessions(Map<String, Object> sessions) {
		this.sessions = sessions;
  }
}
