/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 *
 */
package com.automationanywhere.repository;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.annotations.rules.SelectModes;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.repository.utils.CRConnection;
import com.automationanywhere.repository.utils.CROperations;

import static com.automationanywhere.commandsdk.model.AttributeType.SELECT;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;

/** 
 * @author Stefan Karsten
 */
@BotCommand
@CommandPkg(
		//Unique name inside a package and label to display.
		name = "GetBots", label = "Get Bots", 
		node_label = "Get Bots", description = "Get List of Bots", comment = true ,  text_color = "#83b9c9" , background_color =  "#83b9c9" , icon = "pkg.svg", 
		return_label = "List of Bots", return_type = DataType.DICTIONARY, return_sub_type = STRING ,return_required = true)

public class GetBots {
	
    @Sessions
    private Map<String, Object> sessions;
  
	
	private static final Messages MESSAGES = MessagesFactory
			.getMessages("com.automationanywhere.botcommand.samples.messages");

	//Identify the entry point for the action. Returns a Value<String> because the return type is String. 
	@Execute
	public DictionaryValue action(@Idx(index = "1", type = TEXT)  @Pkg(label = "Session name" , default_value_type = STRING , default_value = "Default") @NotEmpty String sessionName,
			  					 @Idx(index = "2", type = SELECT, options = {
			  							 @Idx.Option(index = "2.1", pkg = @Pkg(label = "Select Workspace", value = "SELECT")), 
			  							 @Idx.Option(index = "2.2", pkg = @Pkg(label = "Workspace String", value = "STRING"))})
       									@Pkg(label = "Workspace", description = "", default_value = "SELECT", default_value_type = STRING)
	   							  @SelectModes  @NotEmpty String workspacetype,
	   							@Idx(index = "2.1.1", type = SELECT, options = {
	   									@Idx.Option(index = "2.1.1.1", pkg = @Pkg(label = "Public", value = "public")),
	   									@Idx.Option(index = "2.1.1.2", pkg = @Pkg(label = "Private", value = "private")) }) @Pkg(label = "Workspace", default_value = "public", default_value_type = STRING) @NotEmpty String workspaceSelect,
	   							@Idx(index = "2.2.1", type = TEXT) @Pkg(label = "Workspace",  description = "public or private", default_value_type = STRING, default_value = "private") @NotEmpty String workspaceString ,
								  @Idx(index = "3", type = TEXT)  @Pkg(label = "Repository Path" , default_value_type = STRING , default_value = "")  String path) throws Exception 
    {

		String workspace;
        if (workspacetype.equals("SELECT")) {
            workspace=  workspaceSelect;
        }
        else {
        	workspace = workspaceString;
        }
		path = (path == null) ? "" : path;
		LinkedHashMap<String,Value> botMap = new LinkedHashMap<String,Value>();
		DictionaryValue result = new DictionaryValue();
		CRConnection connection  = (CRConnection) this.sessions.get(sessionName);  
		CROperations crOp = new CROperations().
							setCrurl(connection.url).
							setToken(connection.token).
							setWorkSpace(workspace).
							setPath(path.replaceAll("/", "\\\\"));
		List<Object> bots = crOp.getBotinFolder();
		
		for (Iterator iterator = bots.iterator(); iterator.hasNext();) {
			JSONObject bot = (JSONObject) iterator.next();
			botMap.put(bot.getString("id"),new StringValue(bot.getString("name")));
		}
		
		result.set(botMap);
		return result;

    }


	public void setSessions(Map<String, Object> sessions) {
		this.sessions = sessions;
   }
}
