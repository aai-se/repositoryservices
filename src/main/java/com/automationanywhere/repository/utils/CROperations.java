package com.automationanywhere.repository.utils;

import java.io.File;
import java.io.FileInputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.repository.GetPackages;





public class CROperations {
	

	
	private static String baseURI = "Automation Anywhere";
	
	private String botId ;
	private String botName  ;
	private String poolId;
	private String poolName;
	private String deviceName;
	private String deviceId;
	private String deploymentId;
	private String executionId;
	private String workspace;
	private String path;
	
	private Map<String,Object> inputVariables;

    private String crurl ;
    private String callbackurl ;
	private String crapikey;
	private String crpassword;
	private String cruser ;
	private String token ;
	private Integer userID;
	
	public CROperations() {
		
		inputVariables = new HashMap<String, Object>();
	
	}
	
	
	public CROperations setToken(String tokenValue) {
		this.token = tokenValue;
		return this;
	}


	public CROperations setCrurl(String  url) {
		this.crurl = url;
		return this;
		
	}
	
	public CROperations setCallbackurl(String  url) {
		this.callbackurl = url;
		return this;
		
	}
	
	public CROperations setWorkSpace(String workspaceName) {
		this.workspace = workspaceName;
		return this;
		
	}
	
	
	public CROperations setPassword(String password) {
		this.crpassword = password;
		return this;
	}
	
	
	public CROperations setCrapikey(String apikey) {
		this.crapikey = apikey;
		return this;
	}
	
	public CROperations setPoolname(String poolname) {
		this.poolName = poolname;
		return this;
	}
	
	public CROperations setDeploymentId(String id) {
		this.deploymentId = id;
		return this;
	}
	
	

	public CROperations setBotname(String botname) {
		this.botName = botname;
		return this;
	}
	
	public CROperations setBotID(String botid) {
		this.botId = botid;
		return this;
	}
	
	public CROperations setDevicename(String devicename) {
		this.deviceName= devicename;
		return this;
	}
	
	public CROperations setInputVariables(Map<String,Object> variables) {
		this.inputVariables = variables;
		return this;
	}
	
	public CROperations setPath(String pathValue) {
		this.path = pathValue;
		return this;
	}
	
	public CROperations setExecutionId(String id) {
		this.executionId = id;
		return this;
	}

	
	public String getBotId() {
		return this.botId;
	}
	
	
	public Integer getUserId() {
		return this.userID;
	}
	
	public String getDeviceId() {
		return this.deviceId;
	}
	
	public String getDeploymentId() {
		return this.deploymentId;
	}
	
	public String getExecutiontId() {
		return this.executionId;
	}
	
	public String getPoolId() {
		return this.poolId;
	}
	
	public String getCallbackURL() {
		return this.callbackurl;
	}
	
	
	public static boolean isInteger(String str)
    {
        for (char c : str.toCharArray())
        {
            if (!Character.isDigit(c)) return false;
        }
        return true;
    }

	
	
	
	public String getAutomationStatus() throws Exception {
  
		String automationStatus = "UNKNOWN";
		URL url = new URL(this.crurl+"/v2/activity/list");
		JSONObject body = new JSONObject();
		JSONObject filter = new JSONObject();
		filter.put("operator", "eq");
		filter.put("field","deploymentId");
		filter.put("value", this.deploymentId);
		body.put("filter",filter);
		String result = HTTPOperations.POSTRequest(url,this.token,body);
		if (!result.contains("POST NOT WORKED")) {
			JSONObject json = new JSONObject(result);
			 JSONArray statusJson = (JSONArray)json.get("list");
			if (!statusJson.isEmpty()) {
				 JSONObject automation = (JSONObject)statusJson.get(0);
				 automationStatus = automation.getString("status");
			}
				
		}
		
		return automationStatus;
	}
	
	
	public String getAutomationUserbyExecutionId() throws Exception {
		  
		String automationuser  = "UNKNOWN";
		URL url = new URL(this.crurl+"/v1/activity/execution/"+this.executionId);
		String result = HTTPOperations.GETRequest(url,this.token);
		if (!result.contains("GET NOT WORKED")) {
			JSONObject json = new JSONObject(result);
			automationuser = json.getString("username");
		}
		
		return automationuser ;
	}

	
	public String getDeviceNamebyExecutionId() throws Exception {
		  
		String deviceName  = "UNKNOWN";
		URL url = new URL(this.crurl+"/v1/activity/execution/"+this.executionId);
		String result = HTTPOperations.GETRequest(url,this.token);
		if (!result.contains("GET NOT WORKED")) {
			JSONObject json = new JSONObject(result);
			deviceName = json.getString("deviceName");
		}
		
		return deviceName ;
	}
	
	public String getDeploymentIdbyExecutionId() throws Exception {
		  
		String deployid  = "UNKNOWN";
		URL url = new URL(this.crurl+"/v1/activity/execution/"+this.executionId);
		String result = HTTPOperations.GETRequest(url,this.token);
		if (!result.contains("GET NOT WORKED")) {
			JSONObject json = new JSONObject(result);
			deployid = json.getString("deploymentId");
		}
		
		return deployid ;
	}
	
	
	public List<Object> getDefaultPackages() throws Exception {
		List<Object> packages = new ArrayList<Object>();
		URL url = new URL(this.crurl+"/v2/packages/package/list");
		JSONObject body = new JSONObject();
		String result = HTTPOperations.POSTRequest(url,this.token,body);
		if (!result.contains("POST NOT WORKED")) {
			JSONObject json = new JSONObject(result);
			 JSONArray packagelist = (JSONArray)json.get("list");
			if (!packagelist.isEmpty()) {
				packagelist.forEach(item -> {
					JSONObject jsonobj = (JSONObject) item;
					packages.add(jsonobj);
				});
				
				 Collections.sort( packages, new Comparator<Object>() {
				        //You can change "Name" with "ID" if you want to sort by ID
				        private static final String KEY_NAME = "name";

				        @Override
				        public int compare(Object a, Object b) {
				            String valA = (String) ((JSONObject) a).get(KEY_NAME);
				            String valB = (String) ((JSONObject) b).get(KEY_NAME);
				            return valA.compareTo(valB);

				        }
				    });
			}	
		}
		
		return packages;
	}
	
	
	
	public List<Object> getAllPackageVersions() throws Exception {
		List<Object> packages = new ArrayList<Object>();
		URL url = new URL(this.crurl+"/v2/packages/package/version/list");
		JSONObject body = new JSONObject();
		JSONObject page = new JSONObject();
		page.put("offset", "0");
		page.put("length", "10000");
		body.put("page",page);
		String result = HTTPOperations.POSTRequest(url,this.token,body);
		if (!result.contains("POST NOT WORKED")) {
			JSONObject json = new JSONObject(result);
			 JSONArray packagelist = (JSONArray)json.get("list");
			if (!packagelist.isEmpty()) {
				packagelist.forEach(item -> {
					JSONObject jsonobj = (JSONObject) item;
					packages.add(jsonobj);
				});
				
				 Collections.sort( packages, new Comparator<Object>() {
				        //You can change "Name" with "ID" if you want to sort by ID
				        private static final String KEY_NAME = "name";

				        @Override
				        public int compare(Object a, Object b) {
				            String valA = (String) ((JSONObject) a).get(KEY_NAME);
				            String valB = (String) ((JSONObject) b).get(KEY_NAME);
				            return valA.compareTo(valB);

				        }
				    });
				
			}	
		}
		
		return packages;
	}

	
	
	public void getUserbyName() throws Exception {
		
		JSONArray botsJson = null;
		if (!isInteger(this.cruser)) {
			URL url = new URL(this.crurl+"/v1/usermanagement/users/list");
			JSONObject body = new JSONObject(); 
			JSONObject filter = new JSONObject();
			filter.put("operator", "eq");
			filter.put("value", this.cruser);
			filter.put("field", "username");
			body.put("filter",filter);
			JSONObject page = new JSONObject();
			page.put("offset", "0");
			page.put("length", "10");
			body.put("page",page);
			String result = HTTPOperations.POSTRequest(url,this.token,body);
			if (!result.contains("POST NOT WORKED")) {
				JSONObject json = new JSONObject(result);
				botsJson = (JSONArray)json.get("list");
			}
	
			if (botsJson != null) {
				botsJson.forEach(item -> {
					JSONObject jsonobj = (JSONObject) item;
					this.userID = jsonobj.getInt("id");
				});
			}
		}
		else {
			this.userID = Integer.parseInt(this.cruser);
		}
	}
	
	
	public List<Object> getBotinFolder() throws Exception {

		List <Object> bots = new ArrayList<Object>();
		JSONArray botsJson = null;

		URL url = new URL(this.crurl+"/v2/repository/workspaces/"+this.workspace+"/files/list");
		JSONObject body = new JSONObject(); 
		JSONObject filter = new JSONObject();
		filter.put("operator", "and");
		JSONArray operands = new JSONArray();
		JSONObject operand1 = new JSONObject();
		operand1.put("value", baseURI+this.path);
		operand1.put("field", "path");
		operand1.put("operator", "substring");
		operands.put(operand1);
		JSONObject operand2 = new JSONObject();
		operand2.put("value", "application/vnd.aa.taskbot");
		operand2.put("field", "type");
		operand2.put("operator", "eq");
		operands.put(operand2);
		filter.put("operands", operands);
		body.put("filter",filter);
		JSONObject page = new JSONObject();
		page.put("offset", "0");
		page.put("length", "10000");
		body.put("page",page);
		String result = HTTPOperations.POSTRequest(url,this.token,body);
		if (!result.contains("POST NOT WORKED")) {
			JSONObject json = new JSONObject(result);
			botsJson = (JSONArray)json.get("list");
		}

    	if (botsJson != null) {
    		botsJson.forEach(item -> {
    			JSONObject jsonobj = (JSONObject) item;
    			bots.add(jsonobj);
    	  });
    		
    		
		 Collections.sort( bots, new Comparator<Object>() {
			        //You can change "Name" with "ID" if you want to sort by ID
			        private static final String KEY_NAME = "name";

			        @Override
			        public int compare(Object a, Object b) {
			            String valA = (String) ((JSONObject) a).get(KEY_NAME);
			            String valB = (String) ((JSONObject) b).get(KEY_NAME);
			            return valA.compareTo(valB);

			        }
			    });
    	}
    	
    	return bots;
	}
	

	
	public String getLatestVersionId(String id) throws Exception {

		String version = "";
		URL url = new URL(this.crurl+"/v2/repository/files/"+id);
		String result = HTTPOperations.GETRequest(url,this.token);
		if (!result.contains("GET NOT WORKED")) {
			JSONObject json = new JSONObject(result);
			version = (String) json.get("versionNumber");
		}
    	return version;
	}
	
	
	public String updateJSONContent(String id, String content) throws Exception {

	
		URL url = new URL(this.crurl+"/v2/repository/files/"+id+"/content?hasErrors=false");
		String result = HTTPOperations.PUTRequest(url,this.token,content);
    	return result;
	}
	
	
	public List<Object> getDependencies(String id) throws Exception {

		JSONArray dependenciesJSON = null;;
		List<Object> dependencies = new ArrayList<Object>();
		URL url = new URL(this.crurl+"/v2/repository/files/"+id+"/dependencies");
		String result = HTTPOperations.GETRequest(url,this.token);
		if (!result.contains("GET NOT WORKED")) {
			JSONObject json = new JSONObject(result);
			dependenciesJSON = (JSONArray)json.get("dependencies");
		}
		
	   	if (dependenciesJSON != null) {
	   		dependenciesJSON.forEach(item -> {
    			JSONObject jsonobj = (JSONObject) item;
    			dependencies.add(jsonobj);
	   		});
	   	}
    	return dependencies;
	}
	
	
	public DictionaryValue getPackageDetails(String packageid) throws Exception {
		
		DictionaryValue packageDetails = new DictionaryValue();
		
		JSONArray packagesJson= null;
		URL url = new URL(this.crurl+"/v2/packages/package/version/list");
		JSONObject body = new JSONObject(); 
		JSONObject filter = new JSONObject();
		filter.put("operator", "eq");
		filter.put("value", packageid);
		filter.put("field", "id");
		body.put("filter",filter);
		JSONObject page = new JSONObject();
		page.put("offset", "0");
		page.put("length", "10000");
		body.put("page",page);
		String result = HTTPOperations.POSTRequest(url,this.token,body);
		if (!result.contains("POST NOT WORKED")) {
			JSONObject json = new JSONObject(result);
			packagesJson = (JSONArray)json.get("list");
		}

    	if (packagesJson != null) {
    		JSONObject packageObject = packagesJson.getJSONObject(0);
    		JSONUtils jutils = new JSONUtils();
    		packageDetails = jutils.parseJSON(packageObject.toString());
    	}
    	
    	return packageDetails;
	}
	
	
	public DictionaryValue getDetails() throws Exception {
		
		JSONArray botsJson=null;
		DictionaryValue botDetails = new DictionaryValue();

		URL url = new URL(this.crurl+"/v2/repository/workspaces/"+this.workspace+"/files/list");
		JSONObject body = new JSONObject(); 
		JSONObject filter = new JSONObject();
		filter.put("operator", "eq");
		filter.put("value", this.botId);
		filter.put("field", "id");
		body.put("filter",filter);
		JSONObject page = new JSONObject();
		page.put("offset", "0");
		page.put("length", "10");
		body.put("page",page);
		String result = HTTPOperations.POSTRequest(url,this.token,body);
		if (!result.contains("POST NOT WORKED")) {
			JSONObject json = new JSONObject(result);
			botsJson = (JSONArray)json.get("list");
		}

    	if (botsJson != null) {
    		JSONObject botJson = botsJson.getJSONObject(0);
    		JSONUtils jutils = new JSONUtils();
    		botDetails = jutils.parseJSON(botJson.toString());
    	}
    	
    	return botDetails;
	}
	
	

	public void getBotbyName() throws Exception {

		JSONArray botsJson = null;
		this.botId = null;
		if ((this.workspace != null) && (this.botName != null)) {
			URL url = new URL(this.crurl+"/v2/repository/workspaces/"+this.workspace+"/files/list");	
			JSONObject body = new JSONObject(); 
			JSONObject filter = new JSONObject();
			filter.put("operator", "and");
			JSONArray operands = new JSONArray();
			JSONObject operand1 = new JSONObject();
			operand1.put("value", this.botName);
			operand1.put("field", "name");	
			operand1.put("operator", "eq");
			operands.put(operand1);
			if (this.path != null) {
				JSONObject operand2 = new JSONObject();
				operand2.put("value", baseURI+this.path);
				operand2.put("field", "path");	
				operand2.put("operator", "substring");
				operands.put(operand2);
			}
			JSONObject operand3 = new JSONObject();
			operand3.put("value", "application/vnd.aa.taskbot");
			operand3.put("field", "type");
			operand3.put("operator", "eq");
			operands.put(operand3);
			filter.put("operands", operands);
			body.put("filter",filter);
			JSONObject page = new JSONObject();
			page.put("offset", "0");
			page.put("length", "10");
			body.put("page",page);
			String result = HTTPOperations.POSTRequest(url,this.token,body);
			
			if (!result.contains("POST NOT WORKED")) {
				JSONObject json = new JSONObject(result);
				botsJson = (JSONArray)json.get("list");
			}

			if (botsJson != null) {
				botsJson.forEach(item -> {
					JSONObject jsonobj = (JSONObject) item;
					String botname = jsonobj.getString("name");
					String id = jsonobj.getString("id");
					this.botId = id;
					this.botName = botname;
				});
			}
		}
	}
	
	
	public void getDevicebyName() throws Exception {

		JSONArray botsJson = null;
		if ((this.deviceName != null)) {
			URL url = new URL(this.crurl+"/v2/repository/devices/list");
			JSONObject body = new JSONObject(); 
			JSONObject filter = new JSONObject();
			filter.put("operator", "eq");
			filter.put("value", this.deviceName);
			filter.put("field", "hostName");
			body.put("filter",filter);
			JSONObject page = new JSONObject();
			page.put("offset", "0");
			page.put("length", "10");
			body.put("page",page);
			String result = HTTPOperations.POSTRequest(url,this.token,body);
			if (!result.contains("POST NOT WORKED")) {
				JSONObject json = new JSONObject(result);
				botsJson = (JSONArray)json.get("list");
			}

			if (botsJson != null) {
				botsJson.forEach(item -> {
					JSONObject jsonobj = (JSONObject) item;
					String id = jsonobj.getString("id");
    			this.deviceId= id;
				});
			}
		}
	}

	

	public void authenticate() throws Exception {
		URL url = new URL(this.crurl+"/v1/authentication");
		JSONObject body = new JSONObject();
		body.put("username", this.cruser);
		if (crpassword != null) {
			body.put("password", this.crpassword);
		}
		else {
			body.put("apikey", this.crapikey);
		}	
		String result = HTTPOperations.POSTRequest(url,null,body);
		if (!result.contains("POST NOT WORKED")) {
			JSONObject Json = new JSONObject(result);
			this.token = Json.get("token").toString();
			this.userID = ((JSONObject)Json.get("user")).getInt("id");
		
		}
	}	 
	
	
	
	public String deployBot(Boolean overrideDefaultDevice) throws Exception {
		JSONObject automation = null;
		this.deploymentId = "";
		if ((this.botId != null) && (this.userID != null)) {
			URL url = new URL(this.crurl+"/v3/automations/deploy");
			JSONObject body = new JSONObject();
			body.put("fileId",this.botId);
			JSONArray userIds = new JSONArray();
			userIds.put(this.userID);
			body.put("runAsUserIds", userIds);	
			body.put("overrideDefaultDevice",overrideDefaultDevice);
			if (this.poolId != null) {
				JSONArray poolIds = new JSONArray();
				poolIds.put(this.poolId);
				body.put("poolIds", poolIds);
			}
			else {;
				deploymentId = "Pool Error";
			}
				
	
			
			if (this.callbackurl != null) {
				JSONObject callback = new JSONObject();
				callback.put("url", this.callbackurl);
				body.put("callbackInfo", callback);
			}
			
			
			
			if (this.inputVariables.size() > 0)
			{
				JSONObject botInput = new JSONObject();
				for (Entry<String,Object> inputvariable : this.inputVariables.entrySet()) {
					JSONObject variable = new JSONObject();
					String classname = inputvariable.getValue().getClass().getSimpleName();
					switch (classname) {
						case "String":
							variable.put("type", "STRING");
							variable.put("string",inputvariable.getValue());
							break;
						case "Boolean":
							variable.put("type", "BOOLEAN");
							variable.put("boolean",inputvariable.getValue());
							break;
						case "Double":
							variable.put("type", "NUMBER");
							variable.put("number",inputvariable.getValue());
							break;
						case "Integer":
							variable.put("type", "NUMBER");
							variable.put("number",inputvariable.getValue());
							break;

						default:
							break;
					}
					
					botInput.put(inputvariable.getKey(), variable);
				}
				
				body.put("botInput", botInput);
				
			}
			
						
			
			String result = HTTPOperations.POSTRequest(url,this.token,body);
			if (!result.contains("POST NOT WORKED")) {
				automation = new JSONObject(result);
				this.deploymentId = automation.getString("deploymentId");
			}
			else {
				deploymentId = "HTTP Error";
			}

			
		}
		else {
			deploymentId = "Bot User Error";
		}
		
		return this.deploymentId;
	}
	
	
	public void getPoolbyName() throws Exception {
		JSONArray pools = null;
		if ((this.poolName != null)) {
			URL url = new URL(this.crurl+"/v2/devices/pools/list");
			JSONObject body = new JSONObject(); 
			JSONObject filter = new JSONObject();
			filter.put("operator", "eq");
			filter.put("value", this.poolName);
			filter.put("field", "name");
			body.put("filter",filter);
			JSONObject page = new JSONObject();
			page.put("offset", "0");
			page.put("length", "100");
			body.put("page",page);
			String result = HTTPOperations.POSTRequest(url,this.token,body);
			if (!result.contains("POST NOT WORKED")) {
				JSONObject json = new JSONObject(result);
				pools = (JSONArray)json.get("list");
			}

	    	if (pools != null) {
	    		pools.forEach(item -> {
	    			JSONObject jsonobj = (JSONObject) item;
	    			String poolname = jsonobj.getString("name");
	    			String id = jsonobj.getString("id");
	    			
	    			if (!poolname.contains(".png")) {
	    				this.poolId = id;
	    			}
	    	  });
	    	}
			
		} 
	}
	
	
	public CROperations setCruser(String user) {
		this.cruser = user;
		return this;
	}


	public void logout() throws Exception {
		if (this.token != null) {
			URL url = new URL(this.crurl+"/v1/authentication/logout");
			String result = HTTPOperations.POSTRequest(url,this.token,null);
		}
	}


	public String deploybyAttendedName() throws Exception {
		JSONObject automation = null;
		this.deploymentId = "";
		if ((this.botId != null) && (this.userID != null)) {
			URL url = new URL(this.crurl+"/v3/automations/deploy");
			JSONObject body = new JSONObject();
			body.put("fileId",this.botId);
			JSONArray userIds = new JSONArray();
			userIds.put(this.userID);
			body.put("runAsUserIds", userIds);	
			body.put("overrideDefaultDevice",false);	
			if (this.callbackurl != null) {
				JSONObject callback = new JSONObject();
				callback.put("url", this.callbackurl);
				body.put("callbackInfo", callback);
			}
			
			
			
			if (this.inputVariables.size() > 0)
			{
				JSONObject botInput = new JSONObject();
				for (Entry<String,Object> inputvariable : this.inputVariables.entrySet()) {
					JSONObject variable = new JSONObject();
					String classname = inputvariable.getValue().getClass().getSimpleName();
					switch (classname) {
						case "String":
							variable.put("type", "STRING");
							variable.put("string",inputvariable.getValue());
							break;
						case "Boolean":
							variable.put("type", "BOOLEAN");
							variable.put("boolean",inputvariable.getValue());
							break;
						case "Double":
							variable.put("type", "NUMBER");
							variable.put("number",inputvariable.getValue());
							break;
						case "Integer":
							variable.put("type", "NUMBER");
							variable.put("number",inputvariable.getValue());
							break;
	
						default:
							break;
					}
					
					botInput.put(inputvariable.getKey(), variable);
				}
				
				body.put("botInput", botInput);
				
			}
			
						
			String result = HTTPOperations.POSTRequest(url,this.token,body);
			if (!result.contains("POST NOT WORKED")) {
				automation = new JSONObject(result);
				this.deploymentId = automation.getString("deploymentId");
			}
			else {
				deploymentId = "HTTP Error";
			}
			
		}
		else {
			deploymentId = "Bot User Error";
		}
		return this.deploymentId;
	}






	public String exportBotContent(String exportname,String id,Boolean includePackages,String filepath ,int retries) throws Exception {
	
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		exportname = exportname+"_"+sdf.format(timestamp);
		
		
		String response = "EXPORT FAILED";
		URL url = new URL(this.crurl+"/v2/blm/export");
		JSONObject body = new JSONObject(); 
		body.put("name", exportname);
		JSONArray fileIds = new JSONArray();
		fileIds.put(Integer.parseInt(id));
		body.put("fileIds",fileIds);
		body.put("includePackages", includePackages);
		
		String result = HTTPOperations.POSTRequest(url,this.token,body);
		String requestId = null;
		if (!result.contains("POST NOT WORKED")) {
			JSONObject json = new JSONObject(result);
			requestId = json.getString("requestId");
		}
	
		if (requestId != null) {
			for (int i = 0; i < retries; i++) {
				Thread.sleep(10000);
				url = new URL(this.crurl+"/v2/blm/status/"+requestId);
				result = HTTPOperations.GETRequest(url,this.token);
				if (!result.contains("GET NOT WORKED")) {
					JSONObject json = new JSONObject(result);
					String status = json.getString("status");
					switch (status) {
					case "COMPLETED" :
						String downloadId = json.getString("downloadFileId");
						url= new URL(this.crurl+"/v2/blm/download/"+downloadId);
						response = DownloadUtility.downloadFile(url,this.token,filepath,exportname+".zip");
						i = retries;
						break;
					case "FAILED" :
						response = "EXPORT FAILED";
						i = retries;
						break;
					default:
						break;
						
					}
				}
				else {
					response = result;
					break;
				}
			}
			
		}
		else {
			response = result;
		}
		return response;

	}
	
	
	

	public List<Object> getProcessesinFolder() throws Exception {

		List <Object> bots = new ArrayList<Object>();
		JSONArray botsJson = null;

		URL url = new URL(this.crurl+"/v2/repository/workspaces/"+this.workspace+"/files/list");
		JSONObject body = new JSONObject(); 
		JSONObject filter = new JSONObject();
		filter.put("operator", "and");
		JSONArray operands = new JSONArray();
		JSONObject operand1 = new JSONObject();
		operand1.put("value", baseURI+this.path);
		operand1.put("field", "path");
		operand1.put("operator", "substring");
		operands.put(operand1);
		JSONObject operand2 = new JSONObject();
		operand2.put("value", "application/vnd.aa.workflow");
		operand2.put("field", "type");
		operand2.put("operator", "eq");
		operands.put(operand2);
		filter.put("operands", operands);
		body.put("filter",filter);
		JSONObject page = new JSONObject();
		page.put("offset", "0");
		page.put("length", "10000");
		body.put("page",page);
		String result = HTTPOperations.POSTRequest(url,this.token,body);
		if (!result.contains("POST NOT WORKED")) {
			JSONObject json = new JSONObject(result);
			botsJson = (JSONArray)json.get("list");
		}

    	if (botsJson != null) {
    		botsJson.forEach(item -> {
    			JSONObject jsonobj = (JSONObject) item;
    			bots.add(jsonobj);
    	  });
    		
    		
		 Collections.sort( bots, new Comparator<Object>() {
			        //You can change "Name" with "ID" if you want to sort by ID
			        private static final String KEY_NAME = "name";

			        @Override
			        public int compare(Object a, Object b) {
			            String valA = (String) ((JSONObject) a).get(KEY_NAME);
			            String valB = (String) ((JSONObject) b).get(KEY_NAME);
			            return valA.compareTo(valB);

			        }
			    });
    	}
    	
    	return bots;
	}
	




	public Boolean hasDependencies(String id) throws Exception {
	
		Boolean hasdependencies = false;
		List<Object> dependencies = new ArrayList<Object>();
		URL url = new URL(this.crurl+"/v2/repository/files/"+id+"/checkout/validate?validateDependencies=false");
		String result = HTTPOperations.GETRequest(url,this.token);
		if (!result.contains("GET NOT WORKED")) {
			JSONObject json = new JSONObject(result);
			hasdependencies =json.getBoolean("hasDependencies");
		}
			
		return hasdependencies;
	}


	public String checkOut(String id, String version, List<String> dependencies) throws Exception {
	
	
		URL url = new URL(this.crurl+"/v2/repository/checkout/"+id);
	    List<String> dependencies_no_dup = new ArrayList<>( new HashSet<>(dependencies));

		JSONObject body = new JSONObject(); 
		JSONArray dependendIDs = new JSONArray();
		for (Iterator iterator = dependencies_no_dup.iterator(); iterator.hasNext();) {
			String string = (String) iterator.next();
			if (!string.equals(id)) {
				dependendIDs.put(string);
			}
		}

		body.put("ids", dependendIDs);
		body.put("versionNumber",version);
		
		String result = HTTPOperations.PUTRequest(url,this.token,body.toString());
		
		if (!result.contains("PUT NOT WORKED") ) {
			List<String> idsList = new ArrayList<String>();
			for (Iterator iterator = dependendIDs.iterator(); iterator.hasNext();) {
				String idcheckout = (String) iterator.next();
				idsList.add(idcheckout);
			}
			result = String.join(",", idsList);
		}
		
		return result;
	}


	public String getJSONContent(String id) throws Exception {
	
		String content = "";
		URL url = new URL(this.crurl+"/v2/repository/files/"+id+"/content");
		String result = HTTPOperations.GETRequest(url,this.token);
		if (!result.contains("GET NOT WORKED")) {
			content = result;
	
		}
		return content;
	}
	
	
	
	public String validate(String id) throws Exception {
		
		String error = "";
		URL url = new URL(this.crurl+"/v1/compiler/bot/"+id);
		String result = HTTPOperations.POSTRequest(url,this.token,null);
		if (!result.contains("POST NOT WORKED")) {
			JSONObject resultObj = new JSONObject(result);
			error = resultObj.getString("error");
			
		}
		else {
			error = "POST NOT WORKED";
		}
		return error;
	}



	public String checkIn(String id, String comment, List<String> dependencies) throws Exception {
	
	
		URL url = new URL(this.crurl+"/v2/repository/checkin");
	    List<String> dependencies_no_dup = new ArrayList<>( new HashSet<>(dependencies));
	
		JSONObject body = new JSONObject(); 
		JSONArray dependendIDs = new JSONArray();
		
		for (Iterator iterator = dependencies_no_dup.iterator(); iterator.hasNext();) {
				String string = (String) iterator.next();
				dependendIDs.put(string);
		}

		dependendIDs.put(id);

		body.put("ids", dependendIDs);
		body.put("description",comment);
		
		String result = HTTPOperations.PUTRequest(url,this.token,body.toString());
		
		if (!result.contains("PUT NOT WORKED") ) {
			List<String> idsList = new ArrayList<String>();
			for (Iterator iterator = dependendIDs.iterator(); iterator.hasNext();) {
				String idcheckin = (String) iterator.next();
				idsList.add(idcheckin);
			}
			result = String.join(",", idsList);
		}
		
		return result;
	}


	public List<Object> getChildren(String id) throws Exception {
	
		JSONArray dependenciesJSON = null;
		List<Object> dependencies = new ArrayList<Object>();
		URL url = new URL(this.crurl+"/v2/repository/files/"+id+"/children");
		String result = HTTPOperations.GETRequest(url,this.token);
		if (!result.contains("GET NOT WORKED")) {
			JSONObject json = new JSONObject(result);
			dependenciesJSON = (JSONArray)json.get("dependencies");
		}
		
	   	if (dependenciesJSON != null) {
	   		dependenciesJSON.forEach(item -> {
				JSONObject jsonobj = (JSONObject) item;
				dependencies.add(jsonobj);
	   		});
	   	}
		return dependencies;
	}








}
