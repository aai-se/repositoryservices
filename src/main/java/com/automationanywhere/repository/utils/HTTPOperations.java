package com.automationanywhere.repository.utils;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import org.json.JSONObject;

public class  HTTPOperations {

	 public static String  POSTRequest(URL posturl, String token, JSONObject body) throws UnsupportedEncodingException, IOException  {

		int responseCode = 0;
		String responseMessage = "";
	    HttpURLConnection postConnection = null;
		try {
			postConnection = (HttpURLConnection) posturl.openConnection();
		    postConnection.setRequestMethod("POST");
		    postConnection.setConnectTimeout(200000);
	        if (token != null)
	        {
	    	    postConnection.setRequestProperty("x-authorization", token);
	        }


		    postConnection.setRequestProperty("Content-Type", "application/json;charset=utf-8");
		    postConnection.setDoOutput(true);
		    if (body != null) {
		     Writer writer = new BufferedWriter(new OutputStreamWriter(postConnection.getOutputStream(), StandardCharsets.UTF_8.name()));

		     writer.write(body.toString());
		     writer.flush();
		     writer.close();
		    }

		    responseCode = postConnection.getResponseCode();
		    responseMessage = postConnection.getResponseMessage();
		    
		} catch (IOException e) {


		}

	    if (responseCode == HttpURLConnection.HTTP_OK || responseCode == HttpURLConnection.HTTP_ACCEPTED) { //success

	    	
	        BufferedReader in = new BufferedReader(new InputStreamReader(

	            postConnection.getInputStream(),StandardCharsets.UTF_8.name()));

	        String inputLine;

	        StringBuffer response = new StringBuffer();

	        while ((inputLine = in .readLine()) != null) {

	            response.append(inputLine);

	        } in .close();

	        
	        return  response.toString() ;

	    } else if ( responseCode == HttpURLConnection.HTTP_NO_CONTENT){
	    	return "OK";
	    } else {
 	        return "POST NOT WORKED "+Integer.valueOf(responseCode).toString() + ":"+ responseMessage;

	    }

	}

	public static String  GETRequest(URL geturl, String token) throws UnsupportedEncodingException, IOException  {
	
		int responseCode = 0;
		String responseMessage = "";
	    HttpURLConnection getConnection = null;
		try {
			getConnection = (HttpURLConnection) geturl.openConnection();
		    getConnection.setRequestMethod("GET");
		    getConnection.setConnectTimeout(30000);
	        if (token != null)
	        {
	        	getConnection.setRequestProperty("x-authorization", token);
	        }
	
	
		    responseCode = getConnection.getResponseCode();
		    responseMessage  = getConnection.getResponseMessage();
		    
		} catch (IOException e) {
	
	
		}
	
	    if (responseCode == HttpURLConnection.HTTP_OK) { //success
	
	        BufferedReader in = new BufferedReader(new InputStreamReader(
	
	            getConnection.getInputStream(),StandardCharsets.UTF_8.name()));
	
	        String inputLine;
	
	        StringBuffer response = new StringBuffer();
	
	        while ((inputLine = in .readLine()) != null) {
	
	            response.append(inputLine);
	
	        } in .close();
	
	        
	        return  response.toString() ;
	
	    } else {

	        return "GET NOT WORKED "+Integer.valueOf(responseCode).toString() + ":"+ responseMessage;
	
	    }
	
	}
	
	
public static String  PUTRequest(URL puturl,String token, String body) throws IOException  {

		
		int responseCode = 0; 
		String responseMessage = "";
	    HttpURLConnection putConnection = null;
		try {
			putConnection = (HttpURLConnection) puturl.openConnection();
			putConnection.setRequestProperty("Content-Type", "application/json;charset=utf-8");
		    putConnection.setRequestMethod("PUT");
        	putConnection.setDoOutput(true);
        	
	        if (token != null)
	        {
	        	putConnection.setRequestProperty("x-authorization", token);
	        }

        	
		    if (body != null) {
			     Writer writer = new BufferedWriter(new OutputStreamWriter(putConnection.getOutputStream(), StandardCharsets.UTF_8.name()));

			     writer.write(body.toString());
			     writer.flush();
			     writer.close();
			}
		    responseCode = putConnection.getResponseCode();
		    responseMessage  = putConnection.getResponseMessage();
		    
		} catch (IOException e) {
			System.out.println(e.getLocalizedMessage());
		}

		
	    if (responseCode == HttpURLConnection.HTTP_OK || responseCode == HttpURLConnection.HTTP_NO_CONTENT ||  responseCode == HttpURLConnection.HTTP_ACCEPTED) { //success
	    	

	        BufferedReader in = new BufferedReader(new InputStreamReader(

	            putConnection.getInputStream(),StandardCharsets.UTF_8.name()));

	        String inputLine;

	        StringBuffer response = new StringBuffer();	

	        while ((inputLine = in .readLine()) != null) {

	            response.append(inputLine);

	        } 
	        in .close();
	        
	        return  response.toString() ;

	    } else {
	    	
	        return "PUT NOT WORKED "+Integer.valueOf(responseCode).toString() + ":"+ responseMessage;


	    }

	}

}
