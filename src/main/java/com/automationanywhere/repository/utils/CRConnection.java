package com.automationanywhere.repository.utils;


public class CRConnection {

	public String url;
	public String token;
	public String executionId;
	
	public CRConnection(String url, String token, String executionid) {
		this.url = url;
		this.token = token;
		this.executionId = executionid;
	}
	
	public String getURL() {
		return this.url;
	}
	
	public String getToken() {
		return this.token ;
	}
	
	public String getExecutionId() {
		return this.executionId;
	}
	
}

