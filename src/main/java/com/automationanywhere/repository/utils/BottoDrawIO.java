package com.automationanywhere.repository.utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Node;

import com.mxgraph.io.mxCodec;
import com.mxgraph.io.mxCodecRegistry;
import com.mxgraph.model.mxCell;
import com.mxgraph.model.mxGeometry;
import com.mxgraph.model.mxGraphModel;
import com.mxgraph.util.mxPoint;
import com.mxgraph.util.mxUtils;
import com.mxgraph.view.mxGraph;
import com.mxgraph.util.mxDomUtils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;






public class BottoDrawIO {
	
	public static final String BOT = "BOT";
	public static final String PROCESS = "PROCESS";

	private mxGraph graph ;
	private mxGraphModel model;
	private static final long deltay = 80;
	private static final long startx = 0;
	private static final long xsize = 100;
	private static final long ysize = 50;
	private static final double ysizeshrink = 1.2;
	private static final long yoffset = 20;
	private static final int spacing = 30;
	private static final long centerx = startx+xsize/2;
	private static final String greenstyle = "rounded=1;fillColor=#b0ffb5;labelPosition=center;verticalLabelPosition=middle;align=center;verticalAlign=middle;arcSize=5;fontStyle=1;fontSize=10";
	private static final String stepcontainerstyle = "swimlane;rounded=1;fillColor=#fcfce1;container=1;labelPosition=center;verticalLabelPosition=top;align=center;verticalAlign=bottom;spacingBottom=-20;recursiveResize=0;arcSize=5;fontStyle=1;fontSize=10";
	private static final String errorcontainerstyle = "swimlane;rounded=1;fillColor=#fcb8a7;container=1;labelPosition=center;verticalLabelPosition=top;align=center;verticalAlign=bottom;spacingBottom=-20;recursiveResize=0;arcSize=5;fontStyle=1;fontSize=10";
	private static final String ifcontainerstyle = "swimlane;rounded=1;fillColor=#fae9b9;container=1;labelPosition=center;verticalLabelPosition=top;align=center;verticalAlign=bottom;spacingBottom=-20;recursiveResize=0;arcSize=5;fontStyle=1;fontSize=10";
	private static final String loopcontainerstyle = "swimlane;rounded=1;fillColor=#dec2f0;container=1;labelPosition=center;verticalLabelPosition=top;align=center;verticalAlign=bottom;spacingBottom=-20;recursiveResize=0;arcSize=5;fontStyle=1;fontSize=10";
	private static final String triggerloopcontainerstyle = "swimlane;rounded=1;fillColor=#c0f0ea;container=1;labelPosition=center;verticalLabelPosition=top;align=center;verticalAlign=bottom;spacingBottom=-20;recursiveResize=0;arcSize=5;fontStyle=1;fontSize=10";
	private static final String triggerloopstyle = "rounded=1;fillColor=#c0f0ea;labelPosition=center;verticalLabelPosition=middle;align=center;verticalAlign=middle;arcSize=5;fontStyle=1;fontSize=10";
	private static final String nodestyle = "rounded=1;fillColor=#c0dcf0;labelPosition=center;verticalLabelPosition=middle;align=center;verticalAlign=middle;arcSize=5;fontStyle=1;fontSize=10";
	private static final String triggerstyle = "rounded=1;fillColor=#d2d6d9;labelPosition=center;verticalLabelPosition=middle;align=center;verticalAlign=middle;arcSize=5;fontStyle=1;fontSize=10";
	private static final String packagestyle = "swimlane;rounded=1;fillColor=#ffffff;labelPosition=center;verticalLabelPosition=top;align=center;verticalAlign=bottom;spacingBottom=-20;recursiveResize=0;arcSize=5;fontStyle=1;fontSize=10";
	private static final String taskbotstyle = "rounded=1;fillColor=#a7f0fb;labelPosition=center;verticalLabelPosition=middle;align=center;verticalAlign=middle;arcSize=5;fontStyle=1;fontSize=10";
	
	private static final String botstepstyle = "shape=ext;rounded=1;html=1;whiteSpace=wrap;labelBackgroundColor=#FFE6CC;fillColor=#FFE6CC;";
	private static final String humanstepstyle = "html=1;whiteSpace=wrap;rounded=1;dropTarget=0;labelBackgroundColor=#D9FBFF;fillColor=#D9FBFF;";
	private static final String processstepstyle = "html=1;whiteSpace=wrap;rounded=1;dropTarget=0;labelBackgroundColor=#CCFF99;fillColor=#CCFF99;";
	private static final String streamstepstyle = "html=1;whiteSpace=wrap;rounded=1;dropTarget=0;labelBackgroundColor=#CCCCFF;fillColor=#CCCCFF;";
	private static final String ifstepstyle = "shape=mxgraph.bpmn.shape;html=1;verticalLabelPosition=bottom;labelBackgroundColor=#ffffff;verticalAlign=top;align=center;perimeter=rhombusPerimeter;background=gateway;outlineConnect=0;outline=none;symbol=exclusiveGw;";
	private static final String startstepstyle ="shape=mxgraph.bpmn.shape;html=1;verticalLabelPosition=bottom;labelBackgroundColor=#ffffff;verticalAlign=top;align=center;perimeter=ellipsePerimeter;outlineConnect=0;outline=standard;symbol=general;";
	private static final String stopstepstyle ="shape=mxgraph.bpmn.shape;html=1;verticalLabelPosition=bottom;labelBackgroundColor=#ffffff;verticalAlign=top;align=center;perimeter=ellipsePerimeter;outlineConnect=0;outline=end;symbol=general;";
	private static final String errorstepstyle ="shape=mxgraph.bpmn.shape;html=1;verticalLabelPosition=bottom;labelBackgroundColor=#ffffff;verticalAlign=top;align=center;perimeter=ellipsePerimeter;outlineConnect=0;outline=end;symbol=error;";
	private static final String cancelstepstyle ="shape=mxgraph.bpmn.shape;html=1;verticalLabelPosition=bottom;labelBackgroundColor=#ffffff;verticalAlign=top;align=center;perimeter=ellipsePerimeter;outlineConnect=0;outline=end;symbol=cancel;";
	private static final String redstyle = "rounded=1;fillColor=#fcb9b8;labelPosition=center;verticalLabelPosition=middle;align=center;verticalAlign=middle;arcSize=5;fontStyle=1;fontSize=10";
	private static final String elbowStyle ="edgeStyle=elbowEdgeStyle";
	private static final String processEdgeStyle =  "edgeStyle=orthogonalEdgeStyle;rounded=0;orthogonalLoop=1;jettySize=auto;html=1;exitX=1;exitY=0.5;exitDx=0;exitDy=0;";
	private String title;
	
	private long x = startx;
	private long y = deltay;
	

	Map<String,mxCell> nodemap ;
	List<ProcessEdge> edgelist ;
	
	private class GraphNode {
		
		private String label;
		private String id;
		private String isBranch;
		
		public  GraphNode(String label,String id,String isBranch)  {
			this.label = label;
			this.id = id;
			this.isBranch = isBranch;
		}
		
		String isBranch() {
			return this.isBranch;
		}
	}
	
	
	private class ProcessEdge {
		
		public ProcessEdge(String start, String end) {
			this.startuid = start;
			this.enduid = end;
		}
		public String startuid;
		public String enduid;
	}
	
	
	public String newDiagram(String name,String json, String type) {

		
		this.graph = new mxGraph()
		{
			// Overrides method to disallow edge label editing
			public boolean isCellEditable(Object cell)
			{
				return !getModel().isEdge(cell);
			}

			// Overrides method to provide a cell label in the display
			public String convertValueToString(Object cell)
			{
				if (cell instanceof mxCell)
				{
					Object value = ((mxCell) cell).getValue();

					if (value instanceof Element)
					{
						Element elt = (Element) value;

						if (elt.getTagName().equalsIgnoreCase("CellObject"))
						{
							String label = elt.getAttribute("label");

							return label;

						}
					}
				}

				return super.convertValueToString(cell);
			}

			// Overrides method to store a cell label in the model
			public void cellLabelChanged(Object cell, Object newValue,
					boolean autoSize)
			{
				if (cell instanceof mxCell && newValue != null)
				{
					Object value = ((mxCell) cell).getValue();

					if (value instanceof Node)
					{
						String label = newValue.toString();
						Element elt = (Element) value;
						if (elt.getTagName().equalsIgnoreCase("CellObject"))
						{
							// Clones the value for correct undo/redo
							elt = (Element) elt.cloneNode(true);

							elt.setAttribute("label", label);

							newValue = elt;
						}
					}
				}

				super.cellLabelChanged(cell, newValue, autoSize);
			}
		};
		
		model = new mxGraphModel();
		graph.setModel(model);
		long x = startx;
		long y = deltay;
		mxCell parent = (mxCell) graph.getDefaultParent();
		if (type == BOT) {
			
			Element value = getCellValueObject("Start", "Node", "",false);
			mxCell startNode = (mxCell)graph.insertVertex(parent, null, value, x, y, xsize,ysize,greenstyle);

			y+=deltay;
			parseJSON(parent,startNode,json,x,y);
			mxCell parentNode = (mxCell) parent;
			mxCell lastNode = null;
			for (int i = 0; i < parentNode.getChildCount(); i++) {
				mxCell child = (mxCell) parentNode.getChildAt(i);
				if (child.isVertex() && child.getAttribute("type") == "Node") {
					lastNode = child;
				}
			}
			if (lastNode != null) {
				Object endNode = graph.insertVertex(parent, null, getCellValueObject("End", "Node", "",false), x, lastNode.getGeometry().getY()+lastNode.getGeometry().getHeight()+spacing, xsize,ysize,redstyle);
				Object e1 = graph.insertEdge(parent, null, "", lastNode, endNode);
			}		

		}
		
		
		if (type == PROCESS) {
			
	
			parseProcessJSON(json,x,y);
			mxCell parentNode = (mxCell) parent;
		   BPMNLayout layout = new BPMNLayout(graph) ;   
		   layout.setUseBoundingBox(false);
		   layout.setLevelDistance(30);
		   layout.setNodeDistance(30);
			layout.execute(parentNode);


		}
		mxCodec encoder = new mxCodec();
		mxCodecRegistry.addPackage("com.automationanywhere.repository.utils"); 
	//	mxCodecRegistry.register(new mxObjectCodec(new com.automationanywhere.repository.utils.BottoDrawIO()));
		Node result = encoder.encode(graph.getModel());

		String xml = mxUtils.getXml(result);
		


		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		Date now = new Date();
	    String strDate = sdf.format(now);
	    UUID uuid = UUID.randomUUID();
		
		String finalxml = "<mxfile host=\"app.diagrams.net\" modified=\""+strDate+"\" agent=\"AAI Bot Documentation\"  compressed=\"false\" version=\"1.0.1\">\r\n" + 
				"  <diagram id=\""+uuid.toString()+"\" name=\""+name+"\">"+xml+"</diagram></mxfile>";
		System.out.println(xml);
		return finalxml;
			
	}
	
	
	private void parseJSON(mxCell parent,mxCell successor, String json,long x, long y) {
	

		int level =0;
		

		JSONObject botlogic = new JSONObject(json);
		if (botlogic != null) {
			JSONArray sublogic;
			if (botlogic.has("packages")) {
				sublogic = botlogic.getJSONArray("packages");
				List<Object> packages = sublogic.toList();
				parsePackagesArray(parent,packages,-720,-80);
			}
			

			if (botlogic.has("triggers")) {
				sublogic = botlogic.getJSONArray("triggers");
				List<Object> triggers= sublogic.toList();
				parseNodeArray(parent,successor,triggers,x,y,1);

			}
		
			if (botlogic.has("nodes")) {
				sublogic = botlogic.getJSONArray("nodes");
				List<Object> commands= sublogic.toList();
				parseNodeArray(parent,successor,commands,x,y,1);
			
			}		
		
		
		}

	}
	

	private void parsePackagesArray(mxCell parent,List<Object> packages,long x, long y ) {
	
		mxCell node ;
		int counter =0;
		mxCell packagenode = (mxCell)graph.insertVertex(parent, null, getCellValueObject("Packages","Packages", "",false), x,y+counter*deltay , xsize*2,ysize,packagestyle);


		for (Iterator iterator = packages.iterator(); iterator.hasNext();) {	
			HashMap object = (HashMap) iterator.next();
			String packageName = (String) object.get("name");
			String version= (String) object.get("version");
			node = (mxCell)graph.insertVertex(packagenode, null, getCellValueObject(packageName+"\n"+version,"Package", "",false),(xsize-50)/2 ,counter*deltay/1.2+30 , xsize+50,ysize,getStyle(packageName,""));
	//		String newstyle = node.getStyle().replace("container=1;labelPosition=center;verticalLabelPosition=top;align=center;verticalAlign=bottom;spacingBottom=-20", "labelPosition=center;verticalLabelPosition=middle;align=center;verticalAlign=middle");
			adjustSwimlaneStyle(node);
			adjustCenterStyle(node);
			counter++;

		}
		mxGeometry geo = packagenode.getGeometry();
		geo.setHeight(geo.getHeight()+10);
		packagenode.setGeometry(geo);
	}

			


	private int parseNodeArray(mxCell parent,mxCell successor,List<Object> commands,long x, long y,int level ) {
	
		mxCell node ;
		int levels  = 0;
		int maxlevels = 0;
		int counter = 0;

		for (Iterator iterator = commands.iterator(); iterator.hasNext();) {	
			HashMap object = (HashMap) iterator.next();
			String  title = null ;
			String packageName = "";
			String triggerName = "";
			String commandName = "";
			List<Object> attributes ;
			String uid = "";
			
			if (object.containsKey("triggerName")) {
				
				packageName = (String) object.get("packageName");
				triggerName = (String) object.get("triggerName");
				attributes = (List<Object>)object.get("attributes");
				title = getAttributeValue(attributes, "title");
				title = (title != null) ? title : triggerName;
				uid = (String) object.get("uid");
				node = (mxCell)graph.insertVertex(parent, uid, getCellValueObject(packageName+":"+title,"Node", object.get("attributes").toString(),true), x+counter*xsize*2,y-3*deltay , xsize,ysize,triggerstyle);
				if (counter > 0 ) {
					mxCell edge = (mxCell)graph.insertEdge(parent, null, "", node,successor,elbowStyle);
					mxGeometry edgeGeo = edge.getGeometry();
					List<mxPoint> points = new ArrayList<mxPoint>();
					points.add(new mxPoint(x+counter*xsize*2-xsize/2,30));
					edgeGeo.setPoints(points);
					edge.setGeometry(edgeGeo);
				}
				else 
				{
					mxCell edge = (mxCell)graph.insertEdge(parent, null, "", node,successor,elbowStyle);
				}
				counter++;
			}
			
			if (object.containsKey("commandName")) {
				packageName = (String) object.get("packageName");
				commandName = (String) object.get("commandName");
				attributes = (List<Object>)object.get("attributes");
				title = getAttributeValue(attributes, "title");
				title = (title != null) ? title : commandName;
				System.out.println(title);
				uid = (String) object.get("uid");

				if (object.containsKey("children")||object.containsKey("branches")) {
					String json = (object.get("attributes") == null) ? "" : new JSONArray((List<Object>)object.get("attributes")).toString();
					node = (mxCell)graph.insertVertex(parent, uid, getCellValueObject(packageName+":"+title,"Node",json,false), x,y, xsize*2,ysize,getStyle(packageName,commandName));
					if (object.containsKey("children")) {
						List<Object> children = (List<Object>)object.get("children");
						int childLevels = parseNodeArray(node,null,children,xsize/2,deltay/3,level+1);
						if (childLevels > maxlevels) {
							maxlevels = childLevels;
						}
						levels = maxlevels;
						int childsCount = node.getChildCount();
						int vertexCount =0;
						int height =0;
						boolean rearrange = true;
						for (int i = 0; i < childsCount; i++) {
							mxCell child = (mxCell) node.getChildAt(i);
				            if (child.isVertex()) {
				            	String type = child.getAttribute("type");
				            	if (type.equals("Branch")) {
				            		rearrange = false;
				            	}
				            	else {    
				            		height = (int)(height + child.getGeometry().getHeight() + 30.0D);
				                    if (child.getGeometry().getX() < 0.0D) {
				                      child.getGeometry().setX(0.0D);
				                    }
				                } 
				            } 
						}
						if (rearrange) {
							for (int i = 0; i < childsCount; i++) {
								mxCell child = (mxCell) node.getChildAt(i);
								if (child.isVertex()) {
									mxGeometry successorGeo = node.getGeometry();
									mxGeometry nodeGeo = child.getGeometry();
									double ws = successorGeo.getWidth();
									double xn = nodeGeo.getX();
									double wn = nodeGeo.getWidth();
									double newx = xn-(xn-ws/2)-wn/2;
									nodeGeo.setX(newx);
									child.setGeometry(nodeGeo);
								}
							}
						
						}
						mxGeometry geometry = node.getGeometry();
						geometry.setHeight(height);
						node.setGeometry(geometry);
						y = y+(long)height+spacing;
					}
					else {
						y = y+(long)ysize+spacing;
					}
						

					if (object.containsKey("branches")) {
						List<Object> branches = (List<Object>)object.get("branches");
						mxCell prebranch = node ;
						mxCell rootbranch = node ;
						List<mxCell> childnodes = new ArrayList<mxCell>();
						int branchcounter =1 ;
						for (Iterator iterator1 = branches.iterator(); iterator1.hasNext();) {	
							HashMap branch = (HashMap) iterator1.next();
							packageName = (String) branch.get("packageName");
							commandName = (String) branch.get("commandName");
							uid = (String) branch.get("uid");		
							json = (branch.get("attributes") == null) ? "" : branch.get("attributes").toString();
							mxCell branchnode = (mxCell)graph.insertVertex(parent, uid, getCellValueObject(packageName+":"+commandName,"Branch", json,false), prebranch.getGeometry().getX()+prebranch.getGeometry().getWidth()+xsize/2,prebranch.getGeometry().getY()+prebranch.getGeometry().getHeight()/2, xsize*2,ysize,getStyle(packageName,commandName));
							childnodes.add(branchnode);
							List<Object> branchchildren = (List<Object>)branch.get("children");
							int childLevels = parseNodeArray(branchnode,null,branchchildren,x,deltay/3,level+1);
							int childsCount = branchnode.getChildCount();//-branchnode.getEdgeCount();
							int height = 0;
							boolean rearrange = true;
							for (int i = 0; i < childsCount; i++) {
								mxCell child = (mxCell) branchnode.getChildAt(i);
								if (child.isVertex()) {
									String type = child.getAttribute("type");
								if (type.equals("Branch")) {
										rearrange = false;
									}
									else
									{
										height = (int) (height+child.getGeometry().getHeight()+spacing);
									}
								}
							}
							
							if (rearrange) {
								for (int i = 0; i < childsCount; i++) {
									mxCell child = (mxCell) branchnode.getChildAt(i);
									if (child.isVertex()) {
										mxGeometry successorGeo = branchnode.getGeometry();
										mxGeometry nodeGeo = child.getGeometry();
										double ws = successorGeo.getWidth();
										double xn = nodeGeo.getX();
										double wn = nodeGeo.getWidth();
										double newx = xn-(xn-ws/2)-wn/2;
										nodeGeo.setX(newx);
										child.setGeometry(nodeGeo) ;
									}
								}
							
							}

							mxGeometry geometry = branchnode.getGeometry();
							height = (int) ((height == 0) ? ysize : height);
							geometry.setHeight(height);
							branchnode.setGeometry(geometry);
							
							mxGeometry prebranchGeo = prebranch.getGeometry();
							mxGeometry branchGeo = branchnode.getGeometry();
							double newy = prebranchGeo.getY();
							branchGeo.setY(newy);
							branchnode.setGeometry(branchGeo) ;
							mxCell e1 = (mxCell)graph.insertEdge(parent, null, "", prebranch , branchnode);	
							e1.setStyle("exitX=1;exitY=0;exitDx=0;exitDy=0;entryX=0;entryY=0;entryDx=0;entryDy=0;entryPerimeter=0");
							branchcounter++;
							prebranch = branchnode;
							
						}
						double maxheight = 0;
						for (Iterator iterator1 = childnodes.iterator(); iterator1.hasNext();) {
							mxCell child = (mxCell) iterator1.next();
							double height = child.getGeometry().getHeight();
							maxheight = (height > maxheight) ? height : maxheight;
						}
						mxGeometry rootNodeGeo = rootbranch.getGeometry();
						double newheight = (rootNodeGeo.getHeight() < maxheight) ? maxheight : rootNodeGeo.getHeight();
						y = (long) (y-rootNodeGeo.getHeight()+newheight);
						rootNodeGeo.setHeight(newheight);
						rootbranch.setGeometry(rootNodeGeo);
						
					}
					
					
				} 
				
				else {
					String json = (object.get("attributes") == null) ? "" : new JSONArray((List<Object>)object.get("attributes")).toString();;
					node = (mxCell) graph.insertVertex(parent, uid, getCellValueObject(packageName+":"+commandName,"Node", json,true), x, y, xsize,ysize,getStyle(packageName,commandName));
					adjustCenterStyle(node);
					y = y+deltay;
				}
				
							
				if (successor != null) {
					Object e1 = graph.insertEdge(parent, null, "", successor, node);
					mxGeometry successorGeo = successor.getGeometry();
					mxGeometry nodeGeo = node.getGeometry();
					double cxs = successorGeo.getCenterX();
					double ws = successorGeo.getWidth();	
					double cxn = nodeGeo.getCenterX();
					double wn = nodeGeo.getWidth();
					double dx = cxn-cxs;
					double newx = cxn-dx-wn/2;
					nodeGeo.setX(newx);
					node.setGeometry(nodeGeo) ;
				}
				else {
					String type = node.getAttribute("type");
					if (type.equals("Branch")) {
						mxGeometry successorGeo = parent.getGeometry();
						mxGeometry nodeGeo = node.getGeometry();
						double ws = successorGeo.getWidth();
						double xn = nodeGeo.getX();
						double wn = nodeGeo.getWidth();
						double newx = xn-(xn-ws/2)-wn/2;
						nodeGeo.setX(newx);
						node.setGeometry(nodeGeo) ;
					}
				}
				successor = node;
	
			}
		}
		return 	levels+1;
	}
	
	
	private String getAttributeValue(List<Object> attributes,String searchName) {

		title= "" ;
		if (attributes != null) {
			attributes.forEach(item -> {
			String attrname = (String) ((HashMap)item).get("name");
			if (attrname.equals(searchName)) {
				title = (String)((HashMap) ((HashMap)item).get("value")).get("string");
				}
	
			});
		}
		
		return title;
	}
	
	

	
	
	
	
	
	private String getStyle(String packagename,String commandname) {
		if (packagename.contains("ErrorHandler")) return errorcontainerstyle;
		if (packagename.contains("Trigger:")) return triggerstyle;
		if (packagename.contains("TriggerLoop") && !commandname.contains("break") ) return triggerloopcontainerstyle;
		if (packagename.contains("TriggerLoop") && commandname.contains("break") ) return triggerloopstyle;
		if (packagename.contains("If")) return ifcontainerstyle;
		if (packagename.contains("Step")) return stepcontainerstyle;
		if (packagename.contains("Loop")) return loopcontainerstyle;
		if (packagename.contains("TaskBot")) return taskbotstyle;
		return nodestyle;
	}
	
	
	private String getProcessStyle(String processActivityType) {
		if (processActivityType.contains("ProcessBOT")) return botstepstyle;
		if (processActivityType.contains("ProcessSTREAM")) return streamstepstyle;
		if (processActivityType.contains("ProcessFORM")) return humanstepstyle;
		if (processActivityType.contains("ProcessSUBPROCESS")) return processstepstyle;
		if (processActivityType.contains("ProcessIF")) return ifstepstyle;
		if (processActivityType.contains("ProcessELSE")) return ifstepstyle;
		if (processActivityType.contains("ProcessINIT")) return startstepstyle;
		if (processActivityType.contains("SUCCESS")) return stopstepstyle;
		if (processActivityType.contains("ERROR")) return errorstepstyle;
		if (processActivityType.contains("CANCELLED")) return cancelstepstyle;
		return nodestyle;
	}
	
	private void adjustCenterStyle(mxCell node) {
		String newstyle = node.getStyle().replace("container=1;labelPosition=center;verticalLabelPosition=top;align=center;verticalAlign=bottom;spacingBottom=-20", "labelPosition=center;verticalLabelPosition=middle;align=center;verticalAlign=middle");
		node.setStyle(newstyle);
	}
	
	private void adjustSwimlaneStyle(mxCell node) {
		String newstyle = node.getStyle().replace("swimlane;","");
		node.setStyle(newstyle);
	}
	
	
	
	
	private Element getCellValueObject(String label, String type, String json,Boolean wrap) {
		Document doc = mxDomUtils.createDocument();
		Element cellObject = doc.createElement("CellObject");
		if (label.contains(":") && wrap) {
			String[] labelparts = label.split(":");
			label = "";
			for (int i = 0; i < labelparts.length; i++) {
				label = label+labelparts[i]+":\n";
			}
			label = label.substring(0, label.length()-2);
		}
		cellObject.setAttribute("label", label);
		cellObject.setAttribute("type", type);
		cellObject.setAttribute("json",json);
		return cellObject;
	}


	private void parseProcessJSON( String json,long x, long y) {
	
	
		nodemap = new HashMap<String,mxCell>();
		edgelist = new ArrayList<ProcessEdge>();
	
		JSONObject botlogic = new JSONObject(json);
		if (botlogic != null) {
			JSONArray sublogic;
			if (botlogic.has("nodes")) {
				sublogic = botlogic.getJSONArray("nodes");
				List<Object> commands= sublogic.toList();
				parseProcessNodeArray(null,commands);
				
				
				for (ProcessEdge edge: edgelist) {
					mxCell startnode = nodemap.get(edge.startuid);
					mxCell endnode =   nodemap.get(edge.enduid);
					if ((startnode != null) && (endnode != null)) {
						mxCell edgeNode = (mxCell) graph.insertEdge(null, null, "", startnode,endnode,processEdgeStyle);
					}
					
				}
			
			}		
		
		
		}
	
	}


	
	
	private void rearrangeCell(mxCell cell,long newx, long newy) {
		mxGeometry geo = cell.getGeometry();
		geo.setX(newx);
		geo.setY(newy);
	}
	
	
	

	private void parseProcessNodeArray(String parentuid,List<Object> commands) {
	
		mxCell node = null ;

		String successorUid = parentuid;
	
		for (Iterator iterator = commands.iterator(); iterator.hasNext();) {	
			HashMap object = (HashMap) iterator.next();
			String packageName = "";
			String commandName = "";
			String uid = "";
			String steptitle = "";

			
			if (object.containsKey("commandName")) {
				packageName = (String) object.get("packageName");
				commandName = (String) object.get("commandName");

				uid = (String) object.get("uid");
	
				String json = (object.get("attributes") == null) ? "" : new JSONArray((List<Object>)object.get("attributes")).toString();;
				switch (commandName) {
					case "BotStep" :	steptitle = getAttributeValue((List<Object>)object.get("attributes"), "stepTitle");
										steptitle = (steptitle != null) ? steptitle : commandName;
										edgelist.add(new ProcessEdge(successorUid,uid));
										node = (mxCell) graph.insertVertex(null, uid, getCellValueObject("Bot Task:"+steptitle,"Step", json,true), x, y, xsize,ysize,getProcessStyle("ProcessBOT"));
										break;
					case "FormStep" :	steptitle = getAttributeValue((List<Object>)object.get("attributes"), "stepTitle");
										steptitle = (steptitle != null) ? steptitle : commandName;
										edgelist.add(new ProcessEdge(successorUid,uid));
										node = (mxCell) graph.insertVertex(null, uid, getCellValueObject("Human Task:"+steptitle,"Step", json,true), x, y, xsize,ysize,getProcessStyle("ProcessFORM"));
										break;
					case "StreamStep" :	steptitle = getAttributeValue((List<Object>)object.get("attributes"), "stepTitle");
										steptitle = (steptitle != null) ? steptitle : commandName;
										edgelist.add(new ProcessEdge(successorUid,uid));
										node = (mxCell) graph.insertVertex(null, uid, getCellValueObject("Filter Task:"+steptitle,"Step", json,true), x, y, xsize,ysize,getProcessStyle("ProcessSTREAM"));
										break;
					case "ProcessStep" :steptitle = getAttributeValue((List<Object>)object.get("attributes"), "stepTitle");
										steptitle = (steptitle != null) ? steptitle : commandName;
										edgelist.add(new ProcessEdge(successorUid,uid));
										node = (mxCell) graph.insertVertex(null, uid, getCellValueObject("Process Task:"+steptitle,"Step", json,true), x, y, xsize,ysize,getProcessStyle("ProcessSUBPROCESS"));
										break;
					case "InitialStep" :steptitle = getAttributeValue((List<Object>)object.get("attributes"), "caseTitle");
										steptitle = (steptitle != null) ? steptitle : commandName;
										edgelist.add(new ProcessEdge(successorUid,uid));
										node = (mxCell) graph.insertVertex(null, uid, getCellValueObject(commandName+":"+steptitle,"Start", json,true), x, y, ysize/ysizeshrink,ysize/ysizeshrink,getProcessStyle("ProcessINIT"));
										break;
					case "schedule" :	steptitle =getAttributeValue((List<Object>)object.get("attributes"), "stepId");
										edgelist.add(new ProcessEdge(parentuid,steptitle));
										break;
					case "exit" 	:	steptitle = getAttributeValue((List<Object>)object.get("attributes"), "caseExit");
										edgelist.add(new ProcessEdge(successorUid,uid));
										node = (mxCell) graph.insertVertex(null, uid, getCellValueObject(steptitle,"End", json,true), x, y, ysize/ysizeshrink,ysize/ysizeshrink,getProcessStyle(steptitle));
										break;

					case "if" 		:	steptitle = getAttributeValue((List<Object>)object.get("attributes"), "stepAlias");
										steptitle = (steptitle== null) ? "" : steptitle;
										edgelist.add(new ProcessEdge(successorUid,uid));
										node = (mxCell) graph.insertVertex(null, uid, getCellValueObject(commandName+":"+steptitle,"If", json,true), x, y, ysize/ysizeshrink,ysize/ysizeshrink,getProcessStyle("ProcessIF"));
										break;
					case "else" 	:	steptitle = getAttributeValue((List<Object>)object.get("attributes"), "stepTitle");
										steptitle = (steptitle == null) ? "" : steptitle;
										edgelist.add(new ProcessEdge(successorUid,uid));
										node = (mxCell) graph.insertVertex(null, uid, getCellValueObject(commandName+":"+steptitle,"Else", json,true), x, y, ysize/ysizeshrink,ysize/ysizeshrink,getProcessStyle("ProcessELSE"));
										break;
					case "elseIf" 	:	steptitle = getAttributeValue((List<Object>)object.get("attributes"), "stepAlias");
										steptitle = (steptitle == null) ? "" : steptitle;
										edgelist.add(new ProcessEdge(successorUid,uid));
										successorUid = uid;
										node = (mxCell) graph.insertVertex(null, uid, getCellValueObject(commandName+":"+steptitle,"Else", json,true), x, y, ysize/ysizeshrink,ysize/ysizeshrink,getProcessStyle("ProcessELSE"));
										break;
				}
										
				if (node != null) {
					y = y+deltay;
					adjustCenterStyle(node);
					nodemap.put(uid, node);
				}
				
				if (object.containsKey("children")) {
					List<Object> children = (List<Object>)object.get("children");
					parseProcessNodeArray(uid,children);	
				}
				
				if (object.containsKey("branches") ) {
					List<Object> branches = (List<Object>)object.get("branches");
					parseProcessNodeArray(uid,branches);
				}
			}
				

		}
		
	}


}
