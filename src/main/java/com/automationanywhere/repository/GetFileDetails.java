/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 *
 */
package com.automationanywhere.repository;


import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.annotations.rules.SelectModes;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.repository.utils.CRConnection;
import com.automationanywhere.repository.utils.CROperations;

import static com.automationanywhere.commandsdk.model.AttributeType.SELECT;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;


import java.util.Map;


/** 
 * @author Stefan Karsten
 */
@BotCommand
@CommandPkg(
		//Unique name inside a package and label to display.
		name = "GetFileDetails", label = "Get File Details", 
		node_label = "Get File Details", description = "Get Details of a File", comment = true ,  text_color = "#83b9c9" , background_color =  "#83b9c9" , icon = "pkg.svg", 
		return_label = "File Details", return_type = DataType.DICTIONARY, return_required = true)

public class GetFileDetails {
	
    @Sessions
    private Map<String, Object> sessions;
  
	
	private static final Messages MESSAGES = MessagesFactory
			.getMessages("com.automationanywhere.botcommand.samples.messages");

	//Identify the entry point for the action. Returns a Value<String> because the return type is String. 
	@Execute
	public DictionaryValue action(@Idx(index = "1", type = TEXT)  @Pkg(label = "Session name" , default_value_type = STRING , default_value = "Default") @NotEmpty String sessionName,
						          @Idx(index = "2", type = TEXT)  @Pkg(label = "File ID" , default_value_type = STRING ) @NotEmpty String id,
						          @Idx(index = "3", type = SELECT, options = {
									       @Idx.Option(index = "3.1", pkg = @Pkg(label = "Select Workspace", value = "SELECT")), 
									       @Idx.Option(index = "3.2", pkg = @Pkg(label = "Workspace String", value = "STRING"))})
								           @Pkg(label = "Workspace", description = "", default_value = "SELECT", default_value_type = STRING)
										   @SelectModes  @NotEmpty String workspacetype,
										   @Idx(index = "3.1.1", type = SELECT, options = {
													@Idx.Option(index = "3.1.1.1", pkg = @Pkg(label = "Public", value = "public")),
													@Idx.Option(index = "3.1.1.2", pkg = @Pkg(label = "Private", value = "private")) }) @Pkg(label = "Workspace", default_value = "public", default_value_type = STRING) @NotEmpty String workspaceSelect,
							          	   @Idx(index = "3.2.1", type = TEXT) @Pkg(label = "Workspace",  description = "public or private", default_value_type = STRING, default_value = "private") @NotEmpty String workspaceString 
							     ) throws Exception 
    {

		String workspace;
        if (workspacetype.equals("SELECT")) {
            workspace=  workspaceSelect;
        }
        else {
        	workspace = workspaceString;
        }
        
		DictionaryValue result = new DictionaryValue();
		CRConnection connection  = (CRConnection) this.sessions.get(sessionName);  
		CROperations crOp = new CROperations().
							setCrurl(connection.url).
							setToken(connection.token).
							setWorkSpace(workspace).
							setBotID(id);
		
		result= crOp.getDetails();
		return result;

    }


	public void setSessions(Map<String, Object> sessions) {
		this.sessions = sessions;
   }
}
