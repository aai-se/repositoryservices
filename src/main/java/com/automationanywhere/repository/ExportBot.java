/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 *
 */
package com.automationanywhere.repository;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.repository.utils.CRConnection;
import com.automationanywhere.repository.utils.CROperations;

import static com.automationanywhere.commandsdk.model.AttributeType.SELECT;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;

/** 
 * @author Stefan Karsten
 */
@BotCommand
@CommandPkg(
		//Unique name inside a package and label to display.
		name = "ExportBot", label = "Export Bot", 
		node_label = "Export Bot", description = "Export a Bot", comment = true ,  text_color = "#83b9c9" , background_color =  "#83b9c9" , icon = "pkg.svg", 
		return_label = "Status", return_type = STRING, return_required = true)

public class ExportBot {
	
    @Sessions
    private Map<String, Object> sessions;
  
	
	private static final Messages MESSAGES = MessagesFactory
			.getMessages("com.automationanywhere.botcommand.samples.messages");

	//Identify the entry point for the action. Returns a Value<String> because the return type is String. 
	@Execute
	public StringValue action(@Idx(index = "1", type = TEXT)  @Pkg(label = "Session name" , default_value_type = STRING , default_value = "Default") @NotEmpty String sessionName,
	          						@Idx(index = "2", type = TEXT)  @Pkg(label = "Filet ID" , default_value_type = STRING ) @NotEmpty String id,
	          						@Idx(index = "3", type = TEXT)  @Pkg(label = "Export Name" , default_value_type = STRING ) @NotEmpty String name,
	          						@Idx(index = "4", type = AttributeType.TEXT)  @Pkg(label = "Path" , default_value_type =DataType.STRING) @NotEmpty String path,
	          						@Idx(index = "5", type = AttributeType.BOOLEAN)  @Pkg(label = "Include Packages" , default_value_type = DataType.BOOLEAN) @NotEmpty Boolean include,
	          						@Idx(index = "6", type = AttributeType.NUMBER)  @Pkg(label = "No of Retries" , default_value_type = DataType.NUMBER) @NotEmpty Number retries
	          						) throws Exception 
    {

		CRConnection connection  = (CRConnection) this.sessions.get(sessionName);  
		CROperations crOp = new CROperations().
							setCrurl(connection.url).
							setToken(connection.token);
		String status = crOp.exportBotContent(name, id, include, path, retries.intValue());

		return new StringValue(status);

    }


	public void setSessions(Map<String, Object> sessions) {
		this.sessions = sessions;
  }
}
