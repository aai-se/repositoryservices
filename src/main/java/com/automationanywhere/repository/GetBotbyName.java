/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 *
 */
package com.automationanywhere.repository;


import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.annotations.rules.SelectModes;

import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.repository.utils.CRConnection;
import com.automationanywhere.repository.utils.CROperations;

import static com.automationanywhere.commandsdk.model.AttributeType.SELECT;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

import java.util.Map;

import org.json.JSONObject;

/** 
 * @author Stefan Karsten
 */
@BotCommand
@CommandPkg(
		//Unique name inside a package and label to display.
		name = "GetBotIDByName", label = "Get Bot ID by Name", 
		node_label = "Get Bot ID by Name", description = "Get Bot ID by Name", comment = true ,  text_color = "#83b9c9" , background_color =  "#83b9c9" , icon = "pkg.svg", 
		return_label = "Bot ID", return_type = DataType.STRING, return_required = true)

public class GetBotbyName {
	
    @Sessions
    private Map<String, Object> sessions;
  
	

	//Identify the entry point for the action. Returns a Value<String> because the return type is String. 
	@Execute
	public StringValue action(@Idx(index = "1", type = TEXT)  @Pkg(label = "Session name" , default_value_type = STRING , default_value = "Default") @NotEmpty String sessionName,
						      @Idx(index = "2", type = TEXT)  @Pkg(label = "File Path" , default_value_type = STRING , description = "If File Name not unique add path , starting with '\\Bots\\'")  String path	,
							  @Idx(index = "3", type = TEXT)  @Pkg(label = "File Name" , default_value_type = STRING ) @NotEmpty String name,
	          				  @Idx(index = "4", type = SELECT, options = {
	          						  @Idx.Option(index = "4.1", pkg = @Pkg(label = "Select Workspace", value = "SELECT")), 
	          						  @Idx.Option(index = "4.2", pkg = @Pkg(label = "Workspace String", value = "STRING"))})
			           					@Pkg(label = "Workspace", description = "", default_value = "SELECT", default_value_type = STRING)
					   			@SelectModes  @NotEmpty String workspacetype,
					   				@Idx(index = "4.1.1", type = SELECT, options = {
					   					@Idx.Option(index = "4.1.1.1", pkg = @Pkg(label = "Public", value = "public")),
					   					@Idx.Option(index = "4.1.1.2", pkg = @Pkg(label = "Private", value = "private")) }) @Pkg(label = "Workspace", default_value = "public", default_value_type = STRING) @NotEmpty String workspaceSelect,
					   				@Idx(index = "4.2.1", type = TEXT) @Pkg(label = "Workspace",  description = "public or private", default_value_type = STRING, default_value = "private") @NotEmpty String workspaceString 
					   		   ) throws Exception 
    {
		
		String workspace;
        if (workspacetype.equals("SELECT")) {
            workspace=  workspaceSelect;
        }
        else {
        	workspace = workspaceString;
        }

		StringValue result = new StringValue();
		CRConnection connection  = (CRConnection) this.sessions.get(sessionName);  
		CROperations crOp = new CROperations().
							setCrurl(connection.url).
							setToken(connection.token).
							setWorkSpace(workspace).
							setBotname(name);
							
							
		if (path.isEmpty()) {
			crOp.setPath(null);
		}
		else {
			crOp.setPath(path.replaceAll("/", "\\\\"));
		}
							
		crOp.getBotbyName();
		String id = crOp.getBotId();
		if (id != null) {
			return new StringValue(id);
		}
		else
		{
			return new StringValue("NOT FOUND");
		}


    }


	public void setSessions(Map<String, Object> sessions) {
		this.sessions = sessions;
   }
}
