/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 *
 */
package com.automationanywhere.repository;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.repository.utils.BottoDrawIO;
import com.automationanywhere.repository.utils.CRConnection;
import com.automationanywhere.repository.utils.CROperations;

import static com.automationanywhere.commandsdk.model.AttributeType.SELECT;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;

/** 
 * @author Stefan Karsten
 */
@BotCommand
@CommandPkg(
		//Unique name inside a package and label to display.
		name = "GetDiagram", label = "Get Diagram", 
		node_label = "Get Diagram", description = "Get Diagram for a Bot or Process in Draw.io format", comment = true ,  text_color = "#83b9c9" , background_color =  "#83b9c9" , icon = "pkg.svg")

public class GetDiagram {
	
    @Sessions
    private Map<String, Object> sessions;
  
	
	private static final Messages MESSAGES = MessagesFactory
			.getMessages("com.automationanywhere.botcommand.samples.messages");

	//Identify the entry point for the action. Returns a Value<String> because the return type is String. 
	@Execute
	public void action(@Idx(index = "1", type = TEXT)  @Pkg(label = "Session name" , default_value_type = STRING , default_value = "Default") @NotEmpty String sessionName,
	          						@Idx(index = "2", type = TEXT)  @Pkg(label = "File ID" , default_value_type = STRING ) @NotEmpty String id,
	          						@Idx(index = "3", type = TEXT)  @Pkg(label = "Diagram name" , default_value_type = STRING ) @NotEmpty String name,
	          						@Idx(index = "4", type = SELECT, options = {
	          								@Idx.Option(index = "4.1", pkg = @Pkg(label = "Public", value = "public")),
	          								@Idx.Option(index = "4.2", pkg = @Pkg(label = "Private", value = "private"))
	          							}) @Pkg(label = "Workspace", default_value = "public", default_value_type = STRING) @NotEmpty String workspace,
	          						@Idx(index = "5", type = TEXT)  @Pkg(label = "File Name" , default_value_type = STRING ) @NotEmpty String filename
	          						) throws Exception 
    {

		CRConnection connection  = (CRConnection) this.sessions.get(sessionName);  
		CROperations crOp = new CROperations().
							setCrurl(connection.url).
							setToken(connection.token).
							setWorkSpace(workspace).
							setBotID(id);
		
		BottoDrawIO drawer = new BottoDrawIO();
		String drawertype = drawer.BOT;
		String content = crOp.getJSONContent(id);
		DictionaryValue details = crOp.getDetails();
		Map<String, Value> detailsMap = details.get();
		Value type = (Value) detailsMap.get("type");  

		if (type != null) {
			if (((StringValue)type).get().equals("application/vnd.aa.workflow")){
				drawertype = drawer.PROCESS;
			}
		}
		String xmlcontent = drawer.newDiagram(name, content,drawertype);
	    BufferedWriter writer = new BufferedWriter(new FileWriter(filename));
	    writer.write(xmlcontent);
	    writer.close();

    }


	public void setSessions(Map<String, Object> sessions) {
		this.sessions = sessions;
  }
}