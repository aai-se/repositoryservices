/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 *
 */
package com.automationanywhere.repository;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;

import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.repository.utils.CRConnection;
import com.automationanywhere.repository.utils.CROperations;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

/** 
 * @author Stefan Karsten
 */

@BotCommand
@CommandPkg(
		//Unique name inside a package and label to display.
		name = "GetAllPackages", label = "Get All Package Versions", 
		node_label = "Get All Package Versions", description = "Get List of All Package Versions", comment = true ,  text_color = "#83b9c9" , background_color =  "#83b9c9" , icon = "pkg.svg", 
		return_label = "List of All Package Versions", return_type = DataType.DICTIONARY, return_sub_type = STRING ,return_required = true)

public class GetAllPackageVersions {
	
    @Sessions
    private Map<String, Object> sessions;
  
	  private static final Logger logger = LogManager.getLogger(GetAllPackageVersions.class);

	@Execute
	public DictionaryValue action(@Idx(index = "1", type = TEXT)  @Pkg(label = "Session name" , default_value_type = STRING , default_value = "Default") @NotEmpty String sessionName
			) throws Exception 
    {

		LinkedHashMap<String,Value> packageMap = new LinkedHashMap<String,Value>();
		DictionaryValue result = new DictionaryValue();
		CRConnection connection  = (CRConnection) this.sessions.get(sessionName);  
		CROperations crOp = new CROperations().
							setCrurl(connection.url).
							setToken(connection.token);
		List<Object> packages = crOp.getAllPackageVersions();
		
		for (Iterator iterator = packages.iterator(); iterator.hasNext();) {
			JSONObject packageJSON= (JSONObject) iterator.next();
			packageMap.put(packageJSON.getString("id"),new StringValue(packageJSON.getString("name")));
		}
		
		result.set(packageMap);
		return result;

    }
	


	public void setSessions(Map<String, Object> sessions) {
		this.sessions = sessions;
	}



}
