/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 *
 */
package com.automationanywhere.repository;

import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.repository.utils.CRConnection;
import com.automationanywhere.repository.utils.CROperations;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;


/** 
 * @author Stefan Karsten
 */
@BotCommand
@CommandPkg(
		//Unique name inside a package and label to display.
		name = "CheckoutFile", label = "Check out File", 
		node_label = "Check out File", description = "Check out a File", comment = true ,  text_color = "#83b9c9" , background_color =  "#83b9c9" , icon = "pkg.svg", 
		return_label = "List of dependencies checked out", return_type = DataType.STRING, return_required = false)

public class CheckoutFile {
	
    @Sessions
    private Map<String, Object> sessions;
  
	
	private static final Messages MESSAGES = MessagesFactory
			.getMessages("com.automationanywhere.botcommand.samples.messages");

	//Identify the entry point for the action. Returns a Value<String> because the return type is String. 
	@Execute
	public StringValue action(@Idx(index = "1", type = TEXT)  @Pkg(label = "Session name" , default_value_type = STRING , default_value = "Default") @NotEmpty String sessionName,
						      @Idx(index = "2", type = TEXT)  @Pkg(label = "File ID" , default_value_type = STRING ) @NotEmpty String id,	
						      @Idx(index = "3", type = AttributeType.BOOLEAN)  @Pkg(label = "Check out dependencies" , default_value_type = DataType.BOOLEAN , default_value = "false" ) @NotEmpty Boolean checkoutDep,
						      @Idx(index = "4", type = AttributeType.NUMBER)  @Pkg(label = "Version to check out" , default_value_type = DataType.NUMBER , description = "If empty latest version will be checked out" ) Number checkoutversion	) throws Exception 
    {


		CRConnection connection  = (CRConnection) this.sessions.get(sessionName);  
		CROperations crOp = new CROperations().
							setCrurl(connection.url).
							setToken(connection.token).
							setWorkSpace("public").
							setBotID(id);
		
		List<String> ids = new ArrayList<String>();
		
		if (checkoutDep) {
			if (crOp.hasDependencies(id) ) {
				List<Object> dependencies = crOp.getDependencies(id);
				for (Iterator iterator = dependencies.iterator(); iterator.hasNext();) {
					JSONObject depObject = (JSONObject) iterator.next();
					String depid = depObject.getString("id");
					System.out.println(depid);
					ids.add(depid);
				}
			}
		}
		
		
		String version;
		if (checkoutversion == null) {
			version = crOp.getLatestVersionId(id);
		}
		else {
			version = Integer.toString(checkoutversion.intValue());
		}
				
		String result = crOp.checkOut(id, version, ids);
		
		
		return new StringValue(result);

    }


	public void setSessions(Map<String, Object> sessions) {
		this.sessions = sessions;
   }
}
